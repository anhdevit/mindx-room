import "bootstrap/dist/css/bootstrap.min.css";
import "./ButtonSubmit.scss";
import React, { Component } from "react";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button
        className="btn"
        disabled={this.props.disabled}
        onClick={() => this.props.onClick()}
      >
        {this.props.buttonName}
      </button>
    );
  }
}

import "bootstrap/dist/css/bootstrap.min.css";
import React, { Component } from "react";

export default class Button extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <input
        type={this.props.type}
        placeholder={this.props.placeholder}
        value={this.props.value}
        onChange={(e) => this.props.onChange(e.target.value)}
      />
    );
  }
}

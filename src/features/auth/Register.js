import React, { Component } from "react";
import loginImg from "../../assets/image/login.svg";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "../../components/buttons/ButtonSubmit";
import Input from "../../components/inputs/Input";
import Axios from "axios";

export class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      confirmPassword: "",
      email: "",
      displayName: "",
      messageError: "",
      buttonDisabled: false,
      successfulRegister: false,
    };
  }

  setInputValue(property, val) {
    val = val.trim();
    if (val.length > 25) return;
    this.setState({
      [property]: val,
    });
  }

  async doRegister() {
    let checkUsername = /^[a-zA-Z0-9]{2,}$/;
    let checkEmail = /^[a-zA-Z][a-zA-Z0-9]+@[\.a-z]{2,}([\.a-z]+)+$/;
    let checkPassword = /^[a-zA-Z0-9]{6,}$/;
    if (
      !checkUsername.test(this.state.username) ||
      !checkPassword.test(this.state.password) ||
      !checkEmail.test(this.state.email) ||
      !this.state.displayName ||
      !this.state.confirmPassword ||
      this.state.password !== this.state.confirmPassword
    ) {
      return;
    }

    this.setState({
      buttonDisabled: true,
    });

    await Axios({
      method: "POST",
      url: "https://study-zoom-xc.herokuapp.com/auth/sign-up",
      data: {
        username: this.state.username,
        displayName: this.state.displayName,
        password: this.state.password,
        confirmPassword: this.state.confirmPassword,
        email: this.state.email,
      },
    })
      .then((res) => {
        this.setState({
            successfulRegister: true,
            messageError: false
        })
      })
      .catch((err) => {
        this.setState({
          buttonDisabled: false,
          messageError: err.response.data,
        });
      });
  }

  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Register</div>
        <div className="content">
          <div className="image">
            <img src={loginImg} />
          </div>
          <div className="form">
            <div className="form-group">
              <Input
                type="text"
                placeholder="Display Name"
                value={this.state.displayName ? this.state.displayName : ""}
                onChange={(val) => this.setInputValue("displayName", val)}
              />
            </div>
            <div className="form-group">
              <Input
                type="email"
                placeholder="Email"
                value={this.state.email ? this.state.email : ""}
                onChange={(val) => this.setInputValue("email", val)}
              />
            </div>
            <div className="form-group">
              <Input
                type="text"
                placeholder="Username"
                value={this.state.username ? this.state.username : ""}
                onChange={(val) => this.setInputValue("username", val)}
              />
            </div>
            <div className="form-group">
              <Input
                type="password"
                placeholder="Password"
                value={this.state.password ? this.state.password : ""}
                onChange={(val) => this.setInputValue("password", val)}
              />
            </div>
            <div className="form-group">
              <Input
                type="password"
                placeholder="Confirm Password"
                value={
                  this.state.confirmPassword ? this.state.confirmPassword : ""
                }
                onChange={(val) => this.setInputValue("confirmPassword", val)}
              />
            </div>
          </div>
        </div>
        <div className="footer">
          <Button
            buttonName="Register"
            disabled={this.state.buttonDisabled}
            onClick={() => this.doRegister()}
          />
          <p>{this.state.messageError ? this.state.messageError : ""}</p>
        </div>
        <p className="register-success">
          {this.state.successfulRegister ? "Bạn đã đăng kí thành công" : ""}
        </p>
      </div>
    );
  }
}

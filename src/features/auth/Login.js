import React, { Component } from "react";
import loginImg from "../../assets/image/login.svg";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "../../components/buttons/ButtonSubmit";
import Input from "../../components/inputs/Input";
import Axios from "axios";

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      buttonDisabled: false,
      messageError: "",
    };

    this.setInputValue = this.setInputValue.bind(this);
    this.doLogin = this.doLogin.bind(this);
  }

  setInputValue(property, val) {
    val = val.trim();
    if (val.length > 25) return;
    this.setState({
      [property]: val,
    });
  }

  async doLogin() {
    if (!this.state.username || !this.state.password) {
      return;
    }
    this.setState({
      buttonDisabled: true,
    });

    await Axios({
      method: "POST",
      url: "https://study-zoom-xc.herokuapp.com/auth/sign-in",
      data: {
        username: this.state.username,
        password: this.state.password,
      },
    })
      .then((res) => {
        this.setState({
          buttonDisabled: false,
          messageError: "",
        });
        console.log(res);

        localStorage.setItem("jwt", res.data.token);
      })
      .catch((err) => {
        let error = err.response.data;
        this.setState({
          buttonDisabled: false,
          messageError: error,
        });
      });
  }

  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Login</div>
        <div className="content">
          <div className="image">
            <img src={loginImg} />
          </div>
          <div className="form">
            <div className="form-group">
              <Input
                type="text"
                placeholder="Username"
                value={this.state.username ? this.state.username : ""}
                onChange={(val) => this.setInputValue("username", val)}
              />
            </div>
            <div className="form-group">
              <Input
                type="password"
                placeholder="Password"
                value={this.state.password ? this.state.password : ""}
                onChange={(val) => this.setInputValue("password", val)}
              />
            </div>
          </div>
        </div>
        <div className="footer">
          <Button
            buttonName="Login"
            disabled={this.state.buttonDisabled}
            onClick={() => this.doLogin()}
          />
        </div>
        <p className="message-error-login">
          {this.state.messageError ? this.state.messageError : ""}
        </p>
      </div>
    );
  }
}

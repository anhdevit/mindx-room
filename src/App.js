import './App.scss'
import React, { Component } from "react";
import Auth from './features/auth/index'

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <Auth />
      </div>
    );
  }
}

export default App;
